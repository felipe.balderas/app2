const pug = require('pug')
const UserAgentParser = require('ua-parser-js')

exports.getRoutesWithId = getRoutesWithId
exports.getRoutes = getRoutes

function getDefaultViewData(req) {
  return {
    errors: req.flash('errors'),
    success: req.flash('success'),
    user: req.user,
    req,
    // UI libraries
    moment: require('moment'),
  }
}

exports.getDefaultViewData = getDefaultViewData

exports.renderView = (req, res, filepath, viewData) => {
  const html = pug.renderFile(`${__dirname}/${filepath}.pug`, viewData)
  return res.send(html)
}

exports.errorHandler = (req, res, errors) => {
  return res.send(errors)
}

exports.getIpFromRequest = (req) => {
  // req.ip is understood as the left-most entry in the X-Forwarded-* header.
  // why this? because on production and stage node is runnig behind a proxy
  // to disable this update /webapp/app.js: app.set('trust proxy', false)
  // the OR statements are for development environments
  const ip = req.ip
    || req.connection.remoteAddress
    || req.connection.socket.remoteAddress
    || req.socket.remoteAddress;
  return ip
}

exports.getUserAgentInfo = (req) => {
  const userAgent = req.headers['user-agent'] || req.headers['User-Agent']
  return new UserAgentParser(userAgent).getResult()
  // returns { ua: '', browser: {}, cpu: {}, device: {}, engine: {}, os: {} }
}

function getRoutesWithId(componentName) {
  return [`/${componentName}/:id`, `/${componentName}s/:id`]
}

function getRoutes(componentName) {
  return [`/${componentName}`, `/${componentName}s`]
}
