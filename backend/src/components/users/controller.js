const {
    validationResult,
  } = require('express-validator/check');
  const spauth = require('node-sp-auth')
  const request = require('request-promise')
  const jwt = require('jsonwebtoken');
  
  const User = require('./model')
  const helper = require('../helper')
  const CONFIG = require('../config')
  
  exports.logout = (req, res) => {
    req.logout();
    req.session.destroy((err) => {
      if (err) {
        console.log('Error : Failed to destroy the session during logout.', err);
      }
      req.user = null;
      res.redirect('/');
    });
  }
  
  // api /api/v1/user/profile
  exports.putProfile = (req, res) => {
    // TODO: add sanitizers and validations
    const errors = validationResult(req);
  
    if (!errors.isEmpty()) {
      const array = errors.array()
      array.forEach((error) => {
        req.flash('errors', error.msg)
      })
      return res.send(array)
    }
  
    const queryOptions = {
      new: true,
    }
  
    console.log('PUT /api/v1/user/profile', req.user.email, req.body)
    return User.findByIdAndUpdate(req.body._id, req.body, queryOptions)
      .then(user => res.send(user))
      .catch(error => helper.errorHandler(req, res, error))
  }
  
  exports.postLogin = (req, res) => {
    const username = req.body.username
    const password = req.body.password
    const user = {
      username,
      password
    }
  
    return spauth.getAuth(`${CONFIG.AUTH.SOFTTEK_SHAREPOINT_URL}sites/dev/`, user)
      .then(auth => getSharepointProfile(auth, username))
      .then(profile => validateUserProfile(profile))
      .then(profile => createJsonToken(profile))
      .then(profile => res.json(profile))
      .catch((error) => {
        console.log(error)
        switch (error) {
          case 'Invalid credentials':
            console.log('Invalid credentials')
            res.status(401).json({ error });
            break;
          case 'User profile doesnt exists':
            console.log('User profile doesnt exists')
            res.status(401).json({ error });
            break;
          default:
            console.log('default')
            res.status(401).json({ error });
        }
      });
  }
  
  function getSharepointProfile(auth, username) {
    auth.headers.Accept = 'application/json;odata=verbose';
    let url = CONFIG.AUTH.SOFTTEK_SHAREPOINT_URL
    url += '_api/web/siteusers/getbyloginname'
    url += `('i%3A0%23.f%7Cmembership%7C${username}')`
    const sharepointProfileCall = {
      headers: auth.headers,
      url,
    }
    return request.get(sharepointProfileCall)
  }
  
  function validateUserProfile(profileString) {
    const profile = JSON.parse(profileString)
    return new Promise((resolve, reject) => {
      User.findOne({ email: profile.d.Email })
        .populate('profilePicture')
        .populate('files')
        .populate('sessionsAsMentee')
        .exec()
        .then((newDoc) => {
          if (newDoc === null || !newDoc) {
            if (process.env.NODE_ENV) {
              return reject('User profile doesnt exists')
            } else {
              const defaultData = {
                email: profile.d.Email,
                profile: {
                  name: profile.d.Title
                }
              }
              return resolve(new User(defaultData).save())
            }
          } else {
            return resolve(newDoc);
          }
        })
        .catch((err) => {
          return reject(err)
        })
    })
  }
  
  function createJsonToken(user) {
    return new Promise((resolve, reject) => {
      const payload = {
        _id: user._id,
      };
      const options = {
        expiresIn: '30 days',
        algorithm: 'HS256',
      };
      jwt.sign(payload, CONFIG.AUTH.SECRET_KEY, options, (err, token) => {
        if (err) return reject(err);
        user = user.toObject({ virtuals: true })
        user.token = token
        return resolve(user);
      });
    });
  }
  