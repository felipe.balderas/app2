module.exports = {
    PUBLIC_DOMAIN_WEB: process.env.WEB_HOST || 'http://localhost:8080',
    PUBLIC: {
      DOMAIN: process.env.API_HOST || 'http://localhost:3000',
      PROTOCOL: 'https',
      APP_DOMAIN: process.env.APP_DOMAIN || 'http://localhost:8080'
    }, 
    AUTH: {
      // JSON WEB TOKEN
      SECRET_KEY: process.env.SECRET_KEY || '5HPY8538RLMMNNpn97Re63IKcRmNxr'
    },
    EMAIL: {
      DEFAULT_FROM: '',
      DEFAULT_SUBJECT: 'App2',
      RECEIVE_ALL_EMAILS: [
        'felipe.balderas@softtek.com'
      ]
    },
    ENV: process.env.ENV || 'DEV',
    PORT: process.env.PORT || 3000,
    SESSION_SECRET: process.env.SESSION_SECRET || 'secretRandomString',
    DATABASE_URL: process.env.DATABASE_URL || 'mongodb://localhost:27017/app2'
  };
  