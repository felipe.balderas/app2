const mongoose = require('mongoose');
const express = require('express');
const session = require('express-session');


//Inizialization
const app = express();
mongoose.connect('mongodb://localhost/app2-db', {
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false
    })
    .then(db => console.log("DB conectada"))
    .catch(err => console.log(err));

//Middlewares
app.use(session({
    secret: 'app2',
    resave: true,
    saveUninitialized: true
})); 

// Express Routes
const components = [
    'users'
]
components.forEach(componentName => addComponentRoutes(componentName));
function addComponentRoutes(componentName) {
    // add router
    app.use('/', require(`./components/${componentName}/router`))
  
    // add odata (singular)
    //odataResource(`/odata/${componentName}`, componentName).initRouter(app)
  
    // add odata (plural)
    /*if (componentName !== 'logs') {
      odataResource(`/odata/${componentName}s`, componentName).initRouter(app)
    }*/
  }
    