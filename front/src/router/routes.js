// Layouts
import MainLayout from 'layouts/MyLayout'
import UserLayout from 'layouts/User'
//  Index
import IndexPage from 'pages/Index'
//  About
import AboutPage from 'pages/About'
// User SignIn
import UserSignInPage from 'pages/SignIn'

const routes = [
  {
    path: '/',
    component: MainLayout,
    children: [
      { path: '', component: IndexPage }
    ]
  }, {
    path: '/about',
    component: MainLayout,
    children: [
      { path: '', component: AboutPage }
    ]
  },
  {
    path: '/user',
    component: UserLayout,
    children: [
      { path: '/user/signin', component: UserSignInPage }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
